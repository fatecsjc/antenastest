#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <math.h> // biblioteca para c�lculos matem�ticos.
#define PI 3.14
float raio[3], x[3], y[3];

void ler_antena(){
    int i; // n�mero da antena
    for(i=1; i <= 3; i++){ // inicializa��o; condi��o e incremento.
        printf("\nDigite o raio da antena %d: ", i);
        scanf("%f", &raio[i]);
        printf("\nDigite as coordenadas da antena %d: ", i);
        scanf("%f, %f", &x[i], &y[i]);
    }
}

float calcular_area(int i){
    float area;
    area = PI * raio[i] * raio[i];
    // area = PI * (pow(raio[i], 2));
    return area;
}

int verificar_cobertura(){
    float xp, yp; // posi��o da pessoa.
    float d[3]; // dist�ncias
    int i;
    printf("\nInforme a posi��o do usu�rio: ");
    scanf("%f, %f", &xp, &yp);

    for(i = 0; i < 3; i++){
        d[i] = sqrt(((x[i] - xp) * (x[i] - xp)) + ((y[i] - yp) * (y[i] - yp)));
    }

    if((d[0] <= raio[0]) || (d[1] <= raio[1]) || (d[2])) return 1;
    else return 0;
}


int main()
{
    setlocale(LC_ALL, "Portuguese");
    printf("Hello mund�o!\n");

    char escolha = '0';
    int antena;

    while(escolha != '4'){
        printf("\nEscolha a op��o:\n 1 - Ler\n 2 - �rea\n 3 - Verificar chamada\n 4 - Sair\n");
        escolha = getchar();

        switch(escolha){
        case '1':
            ler_antena();
            break;
        case '2':
            printf("\nDe qual antena: ");
            scanf("%d", &antena);
            printf("\n�rea: %f", calcular_area(antena));
            break;
        case '3':
            printf("\nLiga��o: %d", verificar_cobertura());
            break;
        case '4':
            printf("\nBye!");
            break;
        default:
            printf("\nOp��o inv�lida.");
        }
    }
    return 0;
}
